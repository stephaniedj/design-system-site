import React, { Component } from 'react';
import Overview from './pages/Overview';

class App extends Component {
  render() {
    return (
      <Overview />
    );
  }
}

export default App;
