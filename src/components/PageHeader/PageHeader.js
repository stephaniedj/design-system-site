import React from 'react';
import * as S from './styled';

const PageHeader = ({children}) => (
 <S.Header className="header">
    <S.SubTitle>Guidelines</ S.SubTitle>
    <S.Title>Accessibility</ S.Title>
    {children}
 </ S.Header>
);

export default PageHeader;