import React from 'react';
import menuContents from '../../data/menuContents';
import SideNavLinks from '../SideNavLinks'
import * as S from './styled';

class SideNavItems extends React.Component {
  constructor(props) {
    super(props)
    this.state = {isVisible: false}
  }

handleClick = (e) => {
  this.setState(state => ({
    isVisible: !state.isVisible
  }));
}

  render() {
    return (
      <S.Content>
        <S.MenuBox  >
          {menuContents.map (content => 
          <S.Menu> 
            <S.DropDown key={content.title} onClick={(e) => this.handleClick(e)}>{content.title}
              {content.items.length > 0 ? <S.Chevron viewBox="0 0 12 7" fill-rule="evenodd">
                <title>Menu dropdown icon</title>
                <path d="M6.002 5.55L11.27 0l.726.685L6.003 7 0 .685.726 0z"></path>
              </S.Chevron> : ''}
            </S.DropDown>
             {this.state.isVisible && 
              <S.MenuList>
                {content.items.map(item => 
              <S.MenuItem >
                <S.Link>{item.name}</S.Link>
              </S.MenuItem>)}
            </S.MenuList>}
          </S.Menu>
          )}
        </S.MenuBox>
        <SideNavLinks />
      </S.Content>
    );
  }
}

export default SideNavItems;



// const SideNavItems = () => (
//   <S.Content>
//    {menuContents.map( content => 
//     <S.MenuBox key={content.title} role='menu' className='side-nav'>
//       <S.Menu role="list" className="menu-title">
//         <S.DropDown className="dropdown" onClick={(e) => handleClick(e)} >{content.title}
//           <S.Chevron role='img' viewBox="0 0 12 7" fill-rule="evenodd">
//           <title>Menu dropdown icon</title>
//           <path d="M6.002 5.55L11.27 0l.726.685L6.003 7 0 .685.726 0z"></path>
//           </S.Chevron>
//         </S.DropDown>
//         <S.MenuList role='menuitem' className="menu-item">
//           {content.items.map (item => 
//           <S.MenuItem key={item.name}>
//             <S.Link href={item.url}>{item.name}</S.Link>
//           </S.MenuItem>)}
//         </S.MenuList>
//       </S.Menu>
//     </S.MenuBox>
//    )}
//     <SideNavLinks />
//   </S.Content>
//   );
  