import React from 'react';
import * as S from './styled';

const Footer = () => (
 <S.FooterContainer>
     <S.FooterContent>
        &copy; 2019 BNNVARA
     </S.FooterContent>
     <S.SocialContainer>
       <S.SocialLink href="https://facebook.com/BNNVARA/" >
        <S.FaceBook />
      </S.SocialLink>
      <S.SocialLink href="https://twitter.com/BNNVARA/">
          <S.Twitter />
      </S.SocialLink>
      <S.SocialLink href="https://instagram.com/omroepbnnvara">
          <S.Instagram />
      </S.SocialLink>
      <S.SocialLink href="https://www.youtube.com/channel/UCbd_cIHnI1J_XcLT86NMyvQ">
          <S.YouTube />
      </S.SocialLink>
    </S.SocialContainer>
 </S.FooterContainer>
);

export default Footer;