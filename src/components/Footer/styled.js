import styled from 'styled-components';

export const FooterContainer = styled.div`
    grid-area: foot;
    box-sizing: border-box;
    background-color: black;
    border-top: 1px solid red;
    margin: 0;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export const FooterContent = styled.p`
    font-size: 1rem;
    color: white;
    margin-left: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const SocialContainer = styled.div `
    margin: 30px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const FaceBook = styled.span`
    background: url(https://cdn.bnnvara.nl/shared/icons/facebook.svg) no-repeat;
`;

export const Twitter = styled.span`
    background: url(https://cdn.bnnvara.nl/shared/icons/twitter.svg) no-repeat;
    margin-left: -5px;
`;

export const Instagram = styled.span`
    background: url(https://cdn.bnnvara.nl/shared/icons/instagram.svg) no-repeat;
`;

export const YouTube = styled.span`
    background: url(https://cdn.bnnvara.nl/shared/icons/youtube.svg) no-repeat;
    padding-top: 5px;
    margin-left: 5px;
`;

export const SocialLink = styled.a`
    text-decoration: none;
    width: 50px;
    height: 50px;

    span {
        display: block;
        height: 100%;
    }
`;
