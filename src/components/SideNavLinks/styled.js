import styled from 'styled-components';

export const LinkBox = styled.div`
    width: 100%;
    padding: 0 1rem;
    display: flex;
    flex-direction: column;
`;

export const Link = styled.a`
    text-decoration: none;
    margin: 0.25rem;
    width: 9rem;
    border: 2px solid red;
    color: black;
    padding: 14px 28px;
    font-size: 1rem;
    cursor: pointer;
    background-color: transparent;
    white-space: nowrap;

    &:hover {
        background: red;
    }
`;