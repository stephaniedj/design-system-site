import styled from 'styled-components';

export const PageNavBox = styled.nav`
    width: auto;
    margin-top: 40px;
    min-height: 2rem;
    height: auto;
    width: 100%;
`;

export const Tablist = styled.ul`
    list-style: none;
    display: flex;
    flex-direction: row;
    padding-inline-start: 0;
`;

export const TabItem = styled.li`
    list-style: none;
    margin-right: 32px;
`;

export const Link = styled.a`
    font-size: 1rem;
    text-decoration: none;
    color: grey;
    font-weight: 600;
    cursor: pointer;
    border-bottom: 15px solid transparent;

    :hover {
        color: black;
    }

    :active {
        box-shadow: 0 4px 0 -1px red;
    }
`;
