import React from 'react';
import * as S from './styled';

const SideNavHeader = () => (
<div className="header">
    <S.Logo aria-current="page" className="logo" href="/">
    CRUX Design System
    </S.Logo>
    <S.Search className="search" role="searchbox" aria-label="Zoeken in de hele website">
      <S.Label>
        <S.Input placeholder="Search"></S.Input>       
      </S.Label>
    </S.Search>
</div>
);

export default SideNavHeader;