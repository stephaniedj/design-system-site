import styled from 'styled-components';

export const SubNav = styled.nav`
    grid-area: sidenav;
    font-size: 14px;
    box-sizing: border-box;
    box-shadow: 0 8px 16px 0 rgba(0,0,0,.1);
    display: flex;
    flex-direction: column;
    background-color: #fff;
    border-right: 2px solid #f4f7fb;
    width: 16rem;
    height: 100%;
    padding: 1rem 0;
    position: fixed;
    z-index: 7000;
    transition: .25s cubic-bezier(.5,0,.1,1);
`;